namespace Tests

open NUnit.Framework

[<TestFixture>]
type Day21Test () =

    [<SetUp>]
    member this.Setup () =
        ()

    [<Test>]
    member this.countChars_noDuplicates_zero () =
        let result = Day2_1.getTwoAndThreeLetterAmount ['a'; 'b'; 'c'] 
        Assert.AreEqual((0, 0), result)

    [<Test>]
    member this.getAmounts_twoSame_correctResult () =
        let result = Day2_1.getTwoAndThreeLetterAmount ['a'; 'a']
        Assert.AreEqual((0, 1), result)
        
    [<Test>]
    member this.getAmounts_threeSame_correctResult () =
        let result = Day2_1.getTwoAndThreeLetterAmount ['a'; 'a'; 'a']
        Assert.AreEqual((1, 0), result)