﻿namespace Tests

open NUnit.Framework

[<TestFixture>]
type FoldWhileTest () =

    [<SetUp>]
    member this.Setup () =
        ()

    [<Test>]
    member this.sumOfFirstFiveNumbers_correctSumIsReturned () =
        let result =
            seq {0 .. 10}
            |> CSeq.foldWhile (fun state element -> state + element) 0 (fun state element -> element < 5)
        Assert.AreEqual(10, result)

    [<Test>]
    member this.emtpySequence_initialStateIsReturned () =
        let result =
            Seq.empty
            |> CSeq.foldWhile (fun state element -> state) 0 (fun state element -> true)
        Assert.AreEqual(0, result)

    [<Test>]
    member this.endIsReached_lastStateIsReturned () =
        let result =
            seq {0 .. 4}
            |> CSeq.foldWhile (fun state element -> state + element) 0 (fun state element -> true)
        Assert.AreEqual(10, result)