﻿namespace Tests

open NUnit.Framework

[<TestFixture>]
type RightTest () =

    [<SetUp>]
    member this.Setup () =
        ()

    [<Test>]
    member this.nonEmptySequence_sequenceWithRightPartOfTupleIsReturned () =
        let result = 
            seq ['a', 1; 'b', 2]
            |> CSeq.right
        let expected = seq [1; 2]
        Assert.AreEqual(expected, result)

    [<Test>]
    member this.emptySequence_emptySequenceIsReturned () =
        let result =
            Seq.empty
            |> CSeq.right
        Assert.IsEmpty(result)