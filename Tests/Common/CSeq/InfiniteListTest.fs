﻿namespace Tests

open NUnit.Framework

[<TestFixture>]
type InfiniteListTest () =

    [<SetUp>]
    member this.Setup () =
        ()

    [<Test>]
    member this.characterBeyondLenghtOfList_correctCharacterIsReturned () =
        let result = 
            CSeq.infiniteList ['a'; 'b']
            |> Seq.item 3
        Assert.AreEqual('b', result)

    [<Test>]
    member this.emptyList_emptySequence () =
        let result = 
            CSeq.infiniteList []
            |> Seq.length
        Assert.AreEqual(0, result)