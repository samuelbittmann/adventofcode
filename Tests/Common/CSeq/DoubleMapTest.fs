﻿namespace Tests

open NUnit.Framework

[<TestFixture>]
type DoubleMapTest () =

    [<SetUp>]
    member this.Setup () =
        ()

    [<Test>]
    member this.nonEmpty_correctMappingForLeft () =
        let (resultLeft, _) =
            CSeq.doubleMap (fun x -> x * 2) (seq [1; 2; 3]) (seq [4; 5; 6])
        let expected = (seq [2; 4; 6])
        Assert.AreEqual(expected, resultLeft)

    [<Test>]
    member this.nonEmpty_correctMappingForRight () =
        let (_, resultRight) =
            CSeq.doubleMap (fun x -> x * 2) (seq [1; 2; 3]) (seq [4; 5; 6])
        let expected = (seq [8; 10; 12])
        Assert.AreEqual(expected, resultRight)

    [<Test>]
    member this.leftEmpty_emptyResultForLeft () =
        let (resultLeft, _) =
            CSeq.doubleMap (fun x -> x * 2) (Seq.empty) (seq [4; 5; 6])
        Assert.IsEmpty(resultLeft)

    [<Test>]
    member this.rightEmpty_emptyResultForRight () =
        let (_, resultRight) =
            CSeq.doubleMap (fun x -> x * 2) (seq [1; 2; 3]) (Seq.empty)
        Assert.IsEmpty(resultRight)