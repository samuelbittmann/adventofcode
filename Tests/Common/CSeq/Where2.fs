﻿namespace Tests

open NUnit.Framework

[<TestFixture>]
type Where2Test () =

    [<SetUp>]
    member this.Setup () =
        ()

    [<Test>]
    member this.noneMatch_emptySequenceIsReturned () =
        let result = CSeq.where2 (fun _ _ -> false) (seq ['a']) (seq ['a'])
        Assert.IsEmpty(result)

    [<Test>]
    member this.allMatch_correctSequenceIsReturned () =
        let result = CSeq.where2 (fun _ _ -> true) (seq ['a'; 'b']) (seq [1; 2])
        let expected = seq ['a', 1; 'b', 2]
        Assert.AreEqual(expected, result)

    [<Test>]
    member this.someMatch_correctSequenceIsReturned () =
        let result = CSeq.where2 (fun left right -> left = right) (seq [1; 2; 3]) (seq [3; 2; 1])
        let expected = seq [2, 2]
        Assert.AreEqual(expected, result)