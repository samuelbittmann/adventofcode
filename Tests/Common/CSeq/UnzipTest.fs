﻿namespace Tests

open NUnit.Framework

[<TestFixture>]
type UnzipTest () =

    [<SetUp>]
    member this.Setup () =
        ()

    [<Test>]
    member this.correctLeftSequence () =
        let (left, _) = (
            seq [1, 'a'; 2, 'b'; 3, 'c']
            |> CSeq.unzip
        )
        let expected = seq [1; 2; 3]
        Assert.AreEqual(expected , left)

    [<Test>]
    member this.correctRightSequence () =
        let (_, right) = (
            seq [1, 'a'; 2, 'b'; 3, 'c']
            |> CSeq.unzip
        )
        let expected = seq ['a'; 'b'; 'c']
        Assert.AreEqual(expected, right)