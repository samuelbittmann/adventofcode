﻿namespace Tests

open NUnit.Framework

[<TestFixture>]
type MapTest () =

    [<SetUp>]
    member this.Setup () =
        ()

    [<Test>]
    member this.none_noneIsReturned () =
        let result = COption.map (fun x -> "result") None
        Assert.AreEqual(None, result)

    [<Test>]
    member this.some_mappedValueIsReturned () =
        let result = COption.map (fun x -> "result") (Some(1))
        Assert.AreEqual(Some("result"), result)