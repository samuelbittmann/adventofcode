﻿namespace Tests

open NUnit.Framework

[<TestFixture>]
type ValueOrDefaultTest () =

    [<SetUp>]
    member this.Setup () =
        ()

    [<Test>]
    member this.none_defaultIsReturned () =
        let result = COption.valueOrDefault "default" None
        Assert.AreEqual("default", result)

    [<Test>]
    member this.some_valueIsReturned () =
        let result = COption.valueOrDefault "default" (Some("value"))
        Assert.AreEqual("value", result)