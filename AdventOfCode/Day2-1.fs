﻿module Day2_1

open System.IO

let rec private countChars (counts: int*int) currentCharCount (input: char list) =
    match input with
        | first :: (second :: rest) when first = second -> countChars counts (currentCharCount + 1) (second :: rest)
        | [] -> counts
        | _ :: rest ->
            let (three, two) = counts
            let newCounts = ((if currentCharCount = 3 then 1 else three), (if currentCharCount = 2 then 1 else two))
            countChars newCounts 1 rest

let getTwoAndThreeLetterAmount (input: char list) =
    input
    |> List.sort
    |> countChars (0, 0) 1

let getResult () =
    File.ReadAllLines(".\Inputs\2-1.txt")
    |> Array.toList
    |> List.map (fun x -> x.Trim())
    |> List.filter (fun x -> not (x = ""))
    |> List.map (fun x -> x.ToCharArray() |> Array.toList)
    |> List.map getTwoAndThreeLetterAmount
    |> List.reduce (fun (aThree, aTwo) (three, two) -> (aThree + three, aTwo + two))
    |> (fun (three, two) -> three * two)
    |> string