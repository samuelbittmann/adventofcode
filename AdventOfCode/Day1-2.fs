﻿module Day1_2

open System.IO

let toInfiniteSequence (l: string list) = Seq.initInfinite (fun i -> l.[i % l.Length])

let getResult () =
    File.ReadAllLines(".\Inputs\1-1.txt")
    |> Array.toList
    |> List.filter (fun x -> not (x = ""))
    |> List.map LanguagePrimitives.ParseInt32
    |> CSeq.infiniteList
    |> CSeq.foldWhile (fun (currentFreq, previousFreq: int Set) element -> (currentFreq + element, previousFreq.Add(currentFreq))) (0, Set.empty) (fun (currentFreq, previousFreq) _ -> not (previousFreq.Contains currentFreq))
    |> (fun (currentFreq, _) -> currentFreq)
    |> string