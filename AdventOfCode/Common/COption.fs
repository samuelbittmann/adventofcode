﻿module COption

let valueOrDefault (defaultValue: 'v) (o: 'v option) =
    match o with
        | Some x -> x
        | None -> defaultValue

let map (mapFunction: 'v -> 'vNew) (o: 'v option) =
    match o with
        | Some x -> (Some (mapFunction x))
        | None -> None