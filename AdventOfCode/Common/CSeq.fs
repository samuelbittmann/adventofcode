﻿module CSeq

open System.Collections.Generic

let infiniteList (source: 'e list) =
    match source with
        | [] -> Seq.empty
        | _ -> Seq.initInfinite (fun i -> source.[i % source.Length])

let rec private foldWhileRec (folder: 'state -> 'e -> 'state) (state: 'state) (predicate: 'state -> 'e -> bool) (source: 'e IEnumerator) =
    let hasMoreValues = source.MoveNext()
    if (hasMoreValues && predicate state source.Current)
        then foldWhileRec folder (folder state source.Current) predicate source
        else state
   
let foldWhile (folder: 'state -> 'e -> 'state) (state: 'state) (predicate: 'state -> 'e -> bool) (source: 'e seq) =
    foldWhileRec folder state predicate (source.GetEnumerator())

let unzip (source: ('a*'b) seq) =
    (
        source |> Seq.map (fun (left, _) -> left),
        source |> Seq.map (fun (_, right) -> right)
    )

let duplicate (source: 'a seq) =
    source
    |> Seq.map (fun e -> (e, e))
    |> unzip

let right (source: ('a*'b) seq) =
    source
    |> Seq.map (fun (_, right) -> right)

let where2 (comparison: 'a -> 'b -> bool) (leftSource: 'a seq) (rightSource: 'b seq) =
    Seq.zip leftSource rightSource
    |> Seq.where (fun (left, right) -> (comparison left right))

let doubleMap (mapping: 'a -> 'b) (leftSource: 'a seq) (rightSource: 'a seq) =
    (
        Seq.map mapping leftSource,
        Seq.map mapping rightSource
    )

let allPairings (source: 'a seq) =
    source
    |> Seq.mapi (fun i x -> (i, x))
    |> duplicate
    ||> Seq.allPairs
    |> Seq.filter (fun ((iLeft, _), (iRight, _)) -> (not (iLeft = iRight)))
    |> Seq.map (fun ((iLeft, left), (iRight, right)) -> (left, right))