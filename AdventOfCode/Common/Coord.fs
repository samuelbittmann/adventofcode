﻿module Coord

type Coord (x: int, y: int) =
    member this.x = x
    member this.y = y