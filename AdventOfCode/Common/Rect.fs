﻿module Rect

open Coord
open System

type Rect (coord: Coord, width: int, height: int) =
    member this.coord = coord
    member this.width = Math.Max(0, width)
    member this.height = Math.Max(0, height)

let createFromBounds left right top bottom =
    new Rect(new Coord(left, top), right - left, bottom - top)

let area(r: Rect) =
    r.width * r.height

let bounds (r: Rect) =
    (r.coord.x, r.coord.x + r.width, r.coord.y, r.coord.y + r.height)

let overlap (left: Rect) (right: Rect) =
    let (lLeft, lRight, lTop, lBottom) = (bounds left)
    let (rLeft, rRight, rTop, rBottom) = (bounds right)
    createFromBounds (Math.Max(lLeft, rLeft)) (Math.Min(lRight, rRight)) (Math.Max(lTop, rTop)) (Math.Min(lBottom, rBottom))