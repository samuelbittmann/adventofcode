﻿module Day2_2

open System.IO
open System

let appendIfEqual (state: string) (left: char) (right: char) =
    if left = right
        then state + left.ToString()
        else state

let getResult () =
    File.ReadAllLines(".\Inputs\2-1.txt")
    |> Array.toSeq
    |> Seq.map (fun x -> x.Trim())
    |> Seq.where (fun x -> not (x = ""))
    |> CSeq.duplicate
    ||> Seq.allPairs
    |> CSeq.unzip
    ||> CSeq.doubleMap (fun x -> x.ToCharArray() |> Array.toList)
    ||> Seq.map2 (fun left right -> (Math.Max(left.Length, right.Length) - 1, List.fold2 appendIfEqual "" left right))
    |> Seq.where (fun (expectedLength, result) -> result.Length = expectedLength)
    |> CSeq.right
    |> Seq.pick (fun x -> Some(x))