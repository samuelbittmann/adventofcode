﻿module Day1_1

open System.IO

let name = "1-1"

let getResult () = 
    File.ReadAllLines(".\Inputs\1-1.txt")
    |> Array.toList
    |> List.map (fun x -> x.Trim())
    |> List.filter (fun x -> not (x = ""))
    |> List.map LanguagePrimitives.ParseInt32
    |> List.reduce (fun accu x -> accu + x)
    |> string