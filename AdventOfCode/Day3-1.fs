﻿module Day3_1

open System.IO
open System.Text.RegularExpressions
open System

let (|Regex|_|) pattern input =
    let m = Regex.Match(input, pattern)
    if m.Success then Some(List.tail [ for g in m.Groups -> g.Value ])
    else None

let trypMapInput (input: string) =
    match input with
        | Regex @"#(\d+)\s+@\s+(\d+),(\d+):\s+(\d+)x(\d+)" [ id; posX; posY; width; height ] -> 
            Some(
                new Rect.Rect(
                    new Coord.Coord(
                        LanguagePrimitives.ParseInt32(posX), 
                        LanguagePrimitives.ParseInt32(posY)
                    ),
                    LanguagePrimitives.ParseInt32(width), 
                    LanguagePrimitives.ParseInt32(height)
                )
            )
        | _ -> None

let rec private calculateOverlappingAreaRec (area: int) (multiplicator: int) (areas: Rect.Rect seq) =
    let overlaps = 
        areas
        |> CSeq.allPairings
        |> Seq.map (fun (left, right) -> Rect.overlap left right)
        |> Seq.filter (fun x -> (Rect.area x) > 0)
    let str = overlaps 
            |> Seq.map (fun x -> (sprintf "[x: %d / y: %d | w: %d / h: %d" x.coord.x x.coord.y x.width x.height))
            |> String.concat "\n"
    Console.WriteLine str
    if (Seq.isEmpty overlaps) then
        area
    else
        calculateOverlappingAreaRec (area + (overlaps |> Seq.fold (fun state v -> state + (Rect.area v)) 0)) (multiplicator * -1) overlaps

let getResult () =
    File.ReadAllLines(".\Inputs\3-1.txt")
    |> Array.toSeq
    |> Seq.map (fun x -> x.Trim())
    |> Seq.where (fun x -> not (x = ""))
    |> Seq.map trypMapInput
    |> Seq.where (fun x -> x.IsSome)
    |> Seq.map (fun x -> x.Value)
    |> calculateOverlappingAreaRec 0 1
    |> string