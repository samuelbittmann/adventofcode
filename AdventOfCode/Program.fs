﻿// Learn more about F# at http://fsharp.org

open System

let exercises =
    [
        "1-1", Day1_1.getResult;
        "1-2", Day1_2.getResult;
        "2-1", Day2_1.getResult;
        "2-2", Day2_2.getResult;
        "3-1", Day3_1.getResult
    ]

let result exerciseName =
    let cb =
        exercises
        |> List.fold (fun accu (name, cb) -> if name = exerciseName then Some(cb) else accu) None
    match cb with
        | Some cb -> cb ()
        | None -> "Invalid exercise"

let rec processInputs () = 
    Console.Clear()
    Console.Write("Select exercise: ")
    let input = Console.ReadLine()
    match input with
        | "q" -> None
        | x -> 
            Console.WriteLine (result x)
            Console.ReadKey() |> ignore
            processInputs()

[<EntryPoint>]
let main argv =
    processInputs() |> ignore
    0 // return an integer exit code
